from django.conf.urls import url
from .views import (
    PartnerListView,
    PartnerDetailView,
    # ProfileEditView,
    ProfileRegisterView,
    ProfileActivateView,
    ProfileLoginView,
    PartnerUpdateView,
    CustomerDetailView,
    CustomerUpdateView,
)

urlpatterns = [
    url(r'^login/$', ProfileLoginView.as_view(), name='login'),
    url(r'^search/$', PartnerListView.as_view(), name='partner_list'),
    url(r'^register/$', ProfileRegisterView.as_view(), name='register'),
    url(r'^activate/(?P<uidb64>[\w-]+)/(?P<token>[\w-]+)$', ProfileActivateView.as_view(), name='activate'),
    url(r'^partner/edit/(?P<slug>[\w-]+)/$', PartnerUpdateView.as_view(), name='edit-partner'),
    url(r'^partner/detail/(?P<slug>[\w-]+)/$', PartnerDetailView.as_view(), name='detail-partner'),
    url(r'^customer/edit/(?P<slug>[\w-]+)/$', CustomerUpdateView.as_view(), name='edit-customer'),
]
