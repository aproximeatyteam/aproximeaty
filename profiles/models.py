from util.utils import unique_slug_generator
from django.db import models
from django.db.models.signals import post_save, pre_save
from accounts.models import AproxUser
from django.utils.translation import ugettext_lazy as _


class Partner(AproxUser):
    name = models.CharField(max_length=20, blank=True, null=True, default='')
    description = models.CharField(max_length=127, blank=True, null=True, default='')
    end_of_service = models.TimeField(blank=True, null=True)
    payment_id = models.CharField(max_length=255, blank=True, null=True)
    is_open = models.BooleanField(default=True)

    class Meta:
        verbose_name = _('Partner')
        verbose_name_plural = _('Partners')

    @property
    def title(self):
        return self.first_name

    def __str__(self):
        return str(self.email)

    def __unicode__(self):
        return str(self.first_name)


def pre_save_partner_model_receiver(sender, instance, **kwargs):
    if instance.pk is None:
        initAccount(instance)


pre_save.connect(pre_save_partner_model_receiver, sender=Partner)


class Customer(AproxUser):
    birthday = models.DateField(_('Birthday'), blank=True, null=True)
    gender = models.CharField(_('Gender'), max_length=1, choices=(
        ('M',  _('male')),
        ('F', _('female')),
        ('o', _('other'))
    ))

    civil_status = models.CharField(_('Civil status'), max_length=1, choices=[
        ('M', _('married')),
        ('S', _('single')),
        ('D', _('divorced')),
        ('W', _('widowed '))
    ])
    payment_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        verbose_name = _('Customer')
        verbose_name_plural = _('Customers')

    @property
    def title(self):
        return self.email

    def __str__(self):
        return str(self.email)

    def __unicode__(self):
        return str(self.email)


def pre_save_customer_model_receiver(sender, instance, **kwargs):
    if instance.pk is None:
        initAccount(instance)


pre_save.connect(pre_save_customer_model_receiver, sender=Customer)


def initAccount(instance):
    instance.is_active = True
    instance.is_email_checked = False
    instance.set_password(instance.password)
    instance.slug = unique_slug_generator(instance)
