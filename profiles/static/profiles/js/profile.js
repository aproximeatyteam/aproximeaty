$(document).ready(function(){
    'use strict';

    var uploadPict = $('#uploadProfPict')
    uploadPict.hide()
    uploadPict.on('change', function(e){
        console.log('changed')
        profileReadURL(this)
    });

    $('#profilePrevImg').on('click', function(){
        uploadPict.click()
    });

    $('#addProfilePict').on('click', function(){
        uploadPict.click()
    });


    function profileReadURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
              $('#profilePrevImg').attr('src', e.target.result)
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
});

function ShowSignUpForm() {
    $('#signInForm').animate('fadeOut', function() {
        $('#signInForm').addClass('d-none');
        $('#signUpForm').removeClass('d-none');
        $('#signUpForm').animate('bounceInUp');
      });

}

function ShowSignInForm() {
    $('#signUpForm').animate('fadeOut', function() {
        $('#signUpForm').addClass('d-none');
        $('#signInForm').removeClass('d-none');
        $('#signInForm').animate('bounceInUp');
      });
}



      