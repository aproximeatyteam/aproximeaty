from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404
from django.db.models import Q
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from .tokens import account_activation_token
from django.core.mail import EmailMultiAlternatives
from django.utils.html import strip_tags
from items.models import Item
from .models import Partner


def activate_email(request, user):
    current_site = get_current_site(request)
    mail_subject = 'Activer votre compte.'
    message = render_to_string('profiles/profile_validation_email.html', {
        'user': user,
        'domain': current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'token': account_activation_token.make_token(user),
        'protocol': 'http'
    })
    text_content = strip_tags(message)
    email = EmailMultiAlternatives(
        mail_subject, text_content, to=[user.email]
    )
    email.attach_alternative(message, "text/html")
    email.send()


def exclude_partners():
    return Partner.objects.exclude(payment_id__isnull=True).exclude(is_active=False)


def update_partner(request, partner):
    if request.POST.get('name'):
        partner.name = request.POST.get('name').strip()

    if request.POST.get('description'):
        partner.description = request.POST.get('description').strip()

    if request.POST.get('address'):
        partner.address = request.POST.get('address').strip()

    if request.POST.get('zip_code'):
        partner.zip_code = request.POST.get('zip_code').strip()

    if request.POST.get('city'):
        partner.city = request.POST.get('city').strip()

    if request.POST.get('country'):
        partner.country = request.POST.get('country').strip()

    if request.FILES.get('profileImage', False):
        partner.image = request.FILES['profileImage']

    if request.POST.get('is-open', False):
        partner.is_open = request.POST.get('is-open')

    the_day_item_slug = request.POST.get('the-day-item-slug')

    update_day_meal(the_day_item_slug, partner)

    partner.save()


def update_day_meal(the_day_item_slug, partner):
    mealsQs = Item.objects.filter(
        ~Q(category_name='meal') and
        Q(partner=partner)
    )

    for meal in mealsQs:
        print('mealsQs')
        if meal.slug == the_day_item_slug.strip():
            print('ldddddddddddddddddddddddddddddddddddd')
            meal.is_the_day_item = True
            meal.is_available = True
        else:
            meal.is_the_day_item = False

        meal.save()
