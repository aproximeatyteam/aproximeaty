from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import Partner, Customer


class PartnerEditForm(forms.ModelForm):

    class Meta:
        model = Partner
        fields = '__all__'


class CustomerEditForm(forms.ModelForm):

    class Meta:
        model = Customer
        fields = [
            'first_name',
            'last_name',
            'birthday',
            'gender',
            'civil_status',
            'address',
            'zip_code',
            'city',
            'country',
            'phone_number'
        ]


class PartnerRegisterForm(forms.ModelForm):

    class Meta:
        model = Partner
        fields = [
            'first_name',
            'last_name',
            'email',
            'password',
            'profile'
        ]

    def save(self, commit=True):
        partner = super(PartnerRegisterForm, self).save(commit=False)
        partner.username = self.cleaned_data.get('email')
        partner.save()
        return partner


class CustomerRegisterForm(forms.ModelForm):

    class Meta:
        model = Customer
        fields = [
            'first_name',
            'last_name',
            'email',
            'password',
            'profile'
        ]

    def save(self, commit=True):
        customer = super(CustomerRegisterForm, self).save(commit=False)
        customer.username = self.cleaned_data.get('email')
        customer.save()
        return customer


class ProfileRegisterForm(forms.Form):
    first_name = forms.CharField(max_length=20, min_length=2)
    last_name = forms.CharField(max_length=20, min_length=2)
    email = forms.EmailField()
    password = forms.CharField(max_length=16, min_length=8)
    profile = forms.CharField(max_length=8, required=True)

    def clean_first_name(self):
        first_name = self.cleaned_data.get('first_name')

        if not first_name:
            raise forms.ValidationError(_('Your firstName is required'))
        return first_name

    def clean_last_name(self):
        last_name = self.cleaned_data.get('last_name')

        if not last_name:
            raise forms.ValidationError(_('Your lastName is required'))
        return last_name

    def clean_email(self):
        email = self.cleaned_data.get('email')

        if not email:
            raise forms.ValidationError(_('Your email address is required'))

        return email

    def clean_password(self):
        password = self.cleaned_data.get('password')

        if not password:
            raise forms.ValidationError(_('Please, enter your password'))

        if len(password) < 8:
            raise forms.ValidationError(_('Your password is small, minimum 8 characters'))

        if len(password) > 16:
            raise forms.ValidationError(_('Your password is long, max 16 characters'))

        return password


class ProfileLoginForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(max_length=16, min_length=8, required=True)


