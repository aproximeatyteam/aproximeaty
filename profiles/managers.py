# from django.db import models
# from django.db.models import Q
#
#
# class ProfileQuerySet(models.query.QuerySet):
#     def search(self, query):
#
#         return self.filter(
#             Q(user__address__icontains=query['address']) |
#             Q(user__zip_code__icontains=query['zip_code']) |
#             Q(user__city__icontains=query['city'])
#         ).distinct()
#
#
# class ProfileManager(models.Manager):
#     address = ''
#     zip_code = ''
#     city = ''
#
#     def get_queryset(self):
#         return ProfileQuerySet(self.model, using=self._db)
#
#     def get_profiles(self, query):
#
#         if query.get('address'):
#             self.address = query.get('address')
#
#         if query.get('zip_code'):
#             self.zip_code = query.get('zip_code')
#
#         if query.get('city'):
#             self.city = query.get('city')
#
#         query = {
#             'address': self.address,
#             'zip_code': self.zip_code,
#             'city': self.city
#         }
#
#         print(query)
#
#         return self.get_queryset().search(query)