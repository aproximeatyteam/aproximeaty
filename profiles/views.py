from django.shortcuts import get_object_or_404, render
from django.views.generic import(
    ListView,
    DetailView,
    UpdateView,
    View
)
from django.db.models import Q
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login
from django.contrib.auth.views import LoginView
from django.utils import timezone
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.mixins import LoginRequiredMixin
from accounts.models import AproxUser
from .tokens import account_activation_token
from items.models import Item
from .models import Partner, Customer
from .forms import (
    ProfileRegisterForm,
    PartnerRegisterForm,
    CustomerRegisterForm,
    ProfileLoginForm,
    PartnerEditForm,
    CustomerEditForm
)
from .utils import activate_email, exclude_partners, update_partner
from checkout.utils import shopping_cart_to_session
from checkout.utils import add_checkout_info_to_context
from checkout.models import ShoppingCart


class PartnerListView(ListView):
    template_name = 'profiles/partner_list.html'
    model = Partner
    paginate_by = 10
    context_object_name = 'partners'

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context = add_checkout_info_to_context(request=self.request, context=context)
        shopping_cart_to_session(self.request)
        return context

    def get_queryset(self):
        shopping_cart_to_session(self.request)
        if self.request.GET.get('address') is None:
            print(' address is none')
            return exclude_partners()
        else:
            _address = self.request.GET.get('address')
            _zip_code = self.request.GET.get('zip_code')
            _city = self.request.GET.get('city')
            partners_excluded = exclude_partners()
            partners = partners_excluded.filter(
                Q(address=_address) |
                Q(zip_code=_zip_code) |
                Q(city=_city)
            )

            return partners


class PartnerUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'profiles/partner_edit.html'
    context_object_name = 'partner'
    form_class = PartnerEditForm
    model = Partner

    def post(self, request, *args, **kwargs):
        try:
            partner = get_object_or_404(Partner, slug=request.user.slug)
            update_partner(request, partner)
            return HttpResponseRedirect(
                reverse_lazy(
                    'dashboard:home'
                )
            )
        except Exception as e:
            print(e)
            return HttpResponseRedirect(
                reverse_lazy(
                    'profile:edit-partner',
                    kwargs={'slug': partner.slug},
                )
            )


class PartnerDetailView(DetailView):
    template_name = 'profiles/partner_detail.html'
    context_object_name = 'partner'
    object = Partner

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context = add_checkout_info_to_context(request=self.request, context=context)
        return context

    def get_object(self, *args, **kwargs):
        partner_slug = self.kwargs.get('slug')
        partner = get_object_or_404(Partner, slug=partner_slug)
        return partner


class CustomerDetailView(DetailView):
    template_name = 'profiles/customer_detail.html'
    context_object_name = 'customer'
    model = Customer

    def get_object(self, *args, **kwargs):
        customer_slug = self.kwargs.get('slug')
        customer = get_object_or_404(Customer, slug=customer_slug)
        return customer


class CustomerUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'profiles/customer_edit.html'
    context_object_name = 'customer'
    fields = [
            'first_name',
            'last_name',
            'birthday',
            'gender',
            'civil_status',
            'address',
            'zip_code',
            'city',
            'country',
            'phone_number'
        ]

    model = Customer
    success_url = reverse_lazy('dashboard:home')


class ProfileRegisterView(View):
    template_name = 'profiles/profile_register.html'
    success_url = reverse_lazy('profile:partner_list')
    form_class = ProfileRegisterForm

    def get(self, request, *args, **kwargs):
        return render(request, template_name=self.template_name)

    def post(self, request, *args, **kwargs):
        form = ProfileRegisterForm(request.POST or None)
        errors = None
        emailExisted = None

        if form.is_valid():
            existed = AproxUser.objects.filter(email__iexact=form.cleaned_data.get('email')).exists()
            print(existed)
            if not existed:
                if request.POST.get('profile') == 'partner':
                    partnerForm = PartnerRegisterForm(request.POST or None)
                    user = partnerForm.save()
                    activate_email(request, user)
                    login(request, user)
                    return HttpResponseRedirect(self.success_url)

                if request.POST.get('profile') == 'customer':
                    customerForm = CustomerRegisterForm(request.POST or None)
                    user = customerForm.save()
                    activate_email(request, user)
                    login(request, user)

                    return HttpResponseRedirect(self.success_url)
            else:
                emailExisted = 'Cet email n\'est pas disponible '

        if form.errors:
            errors = form.errors

        return render(
            request,
            self.template_name, {
                'regForm': form.data,
                'registerErrors': errors,
                'emailExisted': emailExisted
            }
        )


class ProfileActivateView(View):

    def get(self, request, uidb64, token):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = AproxUser.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, AproxUser.DoesNotExist):
            user = None
        if user is not None and account_activation_token.check_token(user, token):
            user.is_active = True
            user.is_email_checked = True
            user.save()
            login(request, user)

            return HttpResponse(
                _('Thank you for your email confirmation. '
                                'Now you can login your account.'
                )
            )
        else:
            return HttpResponse(_('Activation link is invalid!'))


class ProfileLoginView(LoginView):
    form_class = ProfileLoginForm
    template_name = 'profiles/profile_register.html'
    success_url = reverse_lazy('profile:partner_list')

    def get(self, request, *args, **kwargs):
        return render(request, template_name=self.template_name, context={})


    def post(self, request, *args, **kwargs):
        print('ProfileLoginView')
        form = ProfileLoginForm(request.POST or None)
        loginErrors = {}

        if form.is_valid():

            aproxUser = authenticate(
                username=form.cleaned_data.get('email'),
                password=form.cleaned_data.get('password')
            )

            if aproxUser is None:
                loginErrors['notValid'] = _('Your email or password is not valid')

            elif aproxUser.is_active and not aproxUser.is_email_checked:
                diff = abs(aproxUser.date_joined - timezone.now()).days
                if diff > 1:
                    print('diff > 1')
                    loginErrors['notActive'] = _('Please, activate your account')
                elif diff <= 1:
                    print('diff <= 1')
                    login(request, aproxUser)
                    return HttpResponseRedirect(self.success_url)

            elif aproxUser.is_active and aproxUser.is_email_checked:
                print('aproxUser.is_active and aproxUser.email_checked')
                login(request, aproxUser)
                return HttpResponseRedirect(self.success_url)

        if form.errors:
            print(form.errors)
            loginErrors = form.errors

        return render(
            request,
            self.template_name,
            {
                'logForm': form.data,
                'loginErrors': loginErrors
            }
        )

