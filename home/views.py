from django.views import View
from django.urls import reverse_lazy
from django.shortcuts import HttpResponseRedirect


class HomeView(View):
    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(reverse_lazy('profile:partner_list'))
