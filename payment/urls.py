from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import (
    CustomerAddCardView,
    CustomerShowCardView,
    CustomerDefaultCardView,
    CustomerDeleteCardView,
    PartnerAddBankAccountView,
    PartnerShowBankAccountView,
    PartnerDeleteBankAccountView,
    PartnerDefaultBankView
)


urlpatterns = [
    url(r'^customer/$', CustomerShowCardView.as_view(), name='customer-show-card'),
    url(r'^customer/add$', CustomerAddCardView.as_view(), name='customer-add-card'),
    url(r'^customer/set/default/(?P<card_id>[\w-]+)$',
        CustomerDefaultCardView.as_view(),
        name='customer-default-card'),
    url(r'^customer/delete/(?P<card_id>[\w-]+)$',
        CustomerDeleteCardView.as_view(),
        name='customer-delete-card'),
    url(r'^partner/$', PartnerShowBankAccountView.as_view(), name='partner-show-bank-account'),
    url(r'^partner/add$', PartnerAddBankAccountView.as_view(), name='partner-add-bank-account'),
    url(r'^partner/delete/(?P<bank_id>[\w-]+)$',
        PartnerDeleteBankAccountView.as_view(),
        name='partner-delete-bank-account'),
    url(r'^partner/set/default/(?P<bank_id>[\w-]+)$',
        PartnerDefaultBankView.as_view(),
        name='partner-default-bank'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
