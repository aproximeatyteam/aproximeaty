from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.http import JsonResponse
from .utils import (
    add_external_account,
    create_stripe_customer_account,
    get_stripe_customer_acct,
    customer_default_card,
    add_card_to_customer,
    delete_card_to_customer,
    create_connected_account,
    get_stripe_partner_acct,
    delete_bank_account_to_partner,
    partner_default_bank
)
from profiles.models import Customer, Partner


class CustomerDeleteCardView(View):
    template_name = 'payment/customer_show_card.html'

    def get(self, request, *args, **kwargs):
        try:
            customer = Customer.objects.get(id=request.user.id)
            delete_card_to_customer(customer, kwargs.get('card_id'))
            return JsonResponse({'message': 'success'}, status=200)
        except Exception as e:
            return JsonResponse({'error': e}, status=500)


class CustomerDefaultCardView(View):
    template_name = 'payment/customer_show_card.html'

    def get(self, request, *args, **kwargs):
        try:
            customer = Customer.objects.get(id=request.user.id)
            customer_default_card(customer, kwargs.get('card_id'))

            return HttpResponseRedirect(reverse_lazy('payment:customer-show-card'))
        except Exception as e:
            stripe_customer_acct = get_stripe_customer_acct(customer)
            return render(
                request,
                template_name=self.template_name,
                context={'error': e, 'stripe_customer_acct': stripe_customer_acct}
            )


class CustomerShowCardView(View):
    template_name = 'payment/customer_show_card.html'

    def get(self, request):
        try:
            customer = Customer.objects.get(id=request.user.id)
            stripe_customer_acct = get_stripe_customer_acct(customer)
            return render(request,
                          template_name=self.template_name,
                          context={'stripe_customer_acct':stripe_customer_acct}
                          )
        except Exception as e:

            return render(
                request,
                template_name=self.template_name,
                context={'error': e}
            )


class CustomerAddCardView(View):
    template_name = 'payment/customer_add_card.html'

    def get(self, request):
        print('CustomerAddCardView GET')

        return render(
            request,
            template_name=self.template_name,
            context={}
        )

    def post(self, request):
        try:
            card_id = request.POST.get('stripeToken')
            customer = Customer.objects.get(id=request.user.id)
            if customer.payment_id is None:
                create_stripe_customer_account(customer, card_id)
            else:
                add_card_to_customer(customer, card_id)

            return HttpResponseRedirect(reverse_lazy('payment:customer-show-card'))
        except Exception as e:
            return render(
                request,
                template_name=self.template_name,
                context={'error': e}
            )


class PartnerShowBankAccountView(View):
    template_name = 'payment/partner_show_bank_account.html'

    def get(self, request):
        try:
            stripe_partner_acct = get_stripe_partner_acct(request.user.id)
            return render(
                request,
                template_name=self.template_name,
                context={'stripe_partner_acct':stripe_partner_acct}
            )
        except Exception as e:
            return render(
                request,
                template_name=self.template_name,
                context={'error': e}
            )


class PartnerAddBankAccountView(View):
    template_name = 'payment/partner_add_bank_account.html'

    def get(self, request):

        return render(
            request,
            template_name=self.template_name,
            context={'error': ''}
        )

    def post(self, request):
        try:
            bank_id = request.POST.get('stripeToken')
            partner = Partner.objects.get(id=request.user.id)
            if not partner.payment_id:
                create_connected_account(partner, bank_id)
            else:
                add_external_account(partner, bank_id)
            return HttpResponseRedirect(reverse_lazy('payment:partner-show-bank-account'))
        except Exception as e:

            return render(
                request,
                template_name=self.template_name,
                context={'error': e}
            )


class PartnerDeleteBankAccountView(View):
    template_name = 'payment/partner_show_bank_account.html'

    def get(self, request, *args, **kwargs):
        try:
            partner = Partner.objects.get(id=request.user.id)
            delete_bank_account_to_partner(partner, kwargs.get('bank_id'))
            return JsonResponse({'message': 'success'}, status=200)
        except Exception as e:
            return JsonResponse({'error': e}, status=500)


class PartnerDefaultBankView(View):
    template_name = 'payment/partner_show_bank_account.html'

    def get(self, request, *args, **kwargs):
        try:

            partner = Partner.objects.get(id=request.user.id)
            partner_default_bank(partner, kwargs.get('bank_id'))
            return HttpResponseRedirect(reverse_lazy('payment:partner-show-bank-account'))

        except Exception as e:
            print(e)
            stripe_partner_acct = get_stripe_partner_acct(request.user.id)
            return render(
                request,
                template_name=self.template_name,
                context={'error': e, 'stripe_partner_acct': stripe_partner_acct}
            )