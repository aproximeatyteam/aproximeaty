from django.utils.translation import ugettext_lazy as _
from aproximeaty.settings import *
import stripe
from profiles.models import Partner
stripe.api_key = STRIPE_SECRET_KEY


def get_stripe_customer_acct(customer):
    try:
        cust_stripe_acc = {}
        if customer.payment_id:
            cust_stripe_acc = stripe.Customer.retrieve(customer.payment_id)
        return cust_stripe_acc
    except stripe.error.InvalidRequestError as e:
        raise Exception(_('Error found while getting your payment account, please try again'))
    except stripe.error.AuthenticationError as e:
        raise Exception(_('Authentication with payment service failed, please try again'))
    except stripe.error.StripeError as e:
        raise Exception(_('Error encountered with payment service, please try again'))
    except Exception as e:
        raise Exception(e)


def customer_default_card(customer, token):
    try:
        customer_stripe_acct = get_stripe_customer_acct(customer)
        customer_stripe_acct.default_source = token
        customer_stripe_acct.save()
    except stripe.error.StripeError as e:
        raise Exception(_('Making this card a default one failed, please try again'))
    except Exception as e:
        raise Exception(e)


def create_stripe_customer_account(customer, token):
    try:
        customer_stripe_acct = stripe.Customer.create(source=token)
        customer.payment_id = customer_stripe_acct.id
        customer.save()
    except stripe.error.StripeError as e:
        raise Exception(_('Error found while creating your payment account, please try again'))
    except Exception as e:
        raise Exception(e)


def add_card_to_customer(customer, token):
    try:
        customer_stripe_acct = get_stripe_customer_acct(customer)
        customer_stripe_acct.sources.create(source=token)
    except stripe.error.StripeError as e:
        raise Exception('Erreur Found while adding your credit card, please try again')
    except Exception as e:
        raise Exception(e)


def delete_card_to_customer(customer, token):
    try:
        customer_stripe_acct = get_stripe_customer_acct(customer)
        customer_stripe_acct.sources.retrieve(token).delete()
    except stripe.error.StripeError as e:
        raise Exception(_('Erreur found while deleting this card, please try again'))
    except Exception as e:
        raise Exception(e)


def create_account_token(partner):
    try:
        token = stripe.Token.create(
            account={
                'legal_entity': {
                    'first_name': partner.first_name,
                    'last_name': partner.last_name,
                },
                'tos_shown_and_accepted': True,
            },
        )
        return token
    except stripe.error.StripeError as e:
        print(e)
        raise Exception(e)
    except Exception as e:
        raise Exception(e)


def create_connected_account(partner, token):
    try:
        connected_account = stripe.Account.create(
            type="custom",
            account_token=create_account_token(partner).id
        )
        connected_account.external_accounts.create(external_account=token)
        partner.payment_id = connected_account.id
        partner.save()

        return partner
    except stripe.error.StripeError as e:
        print(e)
        raise Exception(_('Error found while creating for your a payment account, please try again'))
    except Exception as e:
        raise Exception(e)


def add_external_account(user, token):
    try:
        account = stripe.Account.retrieve(user.payment_id)
        account.external_accounts.create(external_account=token)
    except stripe.error.InvalidRequestError as e:
        raise Exception(_('Error found while adding new bank account, please try again'))
    except Exception as e:
        raise Exception(e)


def get_stripe_partner_acct(partner_id):
    try:
        connected_account = {}
        partner = Partner.objects.get(id=partner_id)
        if partner.payment_id:
            connected_account = stripe.Account.retrieve(partner.payment_id)
        return connected_account
    except stripe.error.APIConnectionError as e:
        raise Exception(_('Error found while connecting with payment service, please try again'))
    except Exception as e:
        raise Exception(e)


def delete_bank_account_to_partner(partner, bank_id):
    try:
        connected_account = get_stripe_partner_acct(partner.id)
        external_account = connected_account.external_accounts.retrieve(bank_id)
        external_account.delete()
    except stripe.error.StripeError as e:
        raise Exception(_('Error found while deleting your bank account, please try again'))
    except Exception as e:
        raise Exception(e)


def convert_to_cents_than_to_int(number):
    converted_to_cents = float(number) * 100
    converted_to_int = int(converted_to_cents)

    return converted_to_int


def get_money_from_the_card(total, card_id, stripe_customer):
    try:
        total = convert_to_cents_than_to_int(total)
        return stripe.Charge.create(
            amount=total,
            currency="eur",
            customer=stripe_customer,
            source=card_id
        )
    except stripe.error.CardError as e:
        body = e.json_body
        err = body.get('error', {})
        raise Exception(_(err.get('message')+' , please try again'))

    except stripe.error.StripeError as e:
        raise Exception(_('Error found while charging your card, please try again'))
    except Exception as e:
        raise Exception(e)


def charge_customer(amount, token, stripe_account):
    try:
        amount = convert_to_cents_than_to_int(amount)
        result = stripe.Charge.create(
            amount=amount,
            currency="eur",
            source=token,
            application_fee=99,
            stripe_account=stripe_account
        )
        return result
    except stripe.error.CardError as e:
        body = e.json_body
        err = body.get('error', {})
        raise Exception(_(err.get('message')+' , please try again'))
    except stripe.error.StripeError as e:
        print(e)
        raise Exception(_('Error found while charging your card, please try again'))
    except Exception as e:
        raise Exception(e)


def charge_with_shared_customer_token(stripe_customer, stripe_account, card_id):
    try:
        token = stripe.Token.create(
            customer=stripe_customer,
            card=card_id,
            stripe_account=stripe_account
        )
        return token.id
    except stripe.error.StripeError as e:
        print(e)
        raise Exception(_('Error found while generating the token, please try again'))
    except Exception as e:
        raise Exception(e)


def send_money_to_partner(amount, token, stripe_account):
    try:
        return charge_customer(amount, token, stripe_account)
    except Exception as e:
        raise Exception(e)


def send_money_with_shared_cust(amount, stripe_customer, stripe_account, card_id):
    try:
        token = charge_with_shared_customer_token(stripe_customer, stripe_account, card_id)
        return charge_customer(amount, token, stripe_account)
    except Exception as e:
        raise Exception(e)


def partner_default_bank(partner, token):
    try:

        partner_stripe_acct = get_stripe_partner_acct(partner.id)
        bank = partner_stripe_acct.external_accounts.retrieve(token)
        bank.default_for_currency = True
        bank.save()
    except stripe.error.StripeError as e:
        raise Exception(_('Making this bank a default one for this currency failed, please try again'))
    except Exception as e:
        raise Exception(e)