$(document).ready(function(){
    'use strict';


    Stripe.setPublishableKey('pk_test_y4HEZWxCOx70u0toH6kZd6eh');
    var form = $('#customer-credit-card-form');

    form.submit(function(e){
        e.preventDefault();
      Stripe.card.createToken({
          number: $('.card-number').val(),
          cvc: $('.card-cvc').val(),
          exp_month: $('.card-expiry-month').val(),
          exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);

   function stripeResponseHandler(status, response){

        if(response.error){
            form.find('.card-errors').text(response.error.message);
            form.find('button').prop('disabled', false);
        } else {
            var token = response.id;
            form.append($('<input type="hidden" name="stripeToken" />').val(token));
            form.get(0).submit();
        }
   }
    })



});




