$(document).ready(function(){
    'use strict';

    Stripe.setPublishableKey('pk_test_y4HEZWxCOx70u0toH6kZd6eh');
    var form = $('#partner-bank-account-form');

    form.submit(function(e){
        e.preventDefault();

        Stripe.bankAccount.createToken({
            country: $('.country').val(),
            currency: $('.currency').val(),
            account_number: $('.account-number').val(),
            account_holder_name: $('.name').val(),
            account_holder_type: $('.account-holder-type').val(),
        },stripeResponseHandler);

       function stripeResponseHandler(status, response){

            if(response.error){
                console.log(response.error.message)
                form.find('.bank-errors').text(response.error.message);
                form.find('button').prop('disabled', false);
            } else {
                var token = response.id;
                console.log(token)
                form.append($('<input type="hidden" name="stripeToken" />').val(token));
                form.get(0).submit();
            }
       }
    });

  });
