$(document).ready(function(){
    'use strict';

    $(".delete-bank").click(function(e) {
        e.preventDefault();
        $("#confirm-delete-bank").modal("show");
        console.log('deleted ddd')
        var bank_id = $(this).attr("id");
        console.log(bank_id)
        $("#btn-ok").attr("href", "/payment/partner/delete/" + bank_id);
        $("#btn-ok").click(function(e) {
          e.preventDefault();
          deleteBank($("#btn-ok").attr("href"));
        });

    });

    function deleteBank(_url){
        console.log(_url)
         $.ajax({
              type: "GET",
              url: _url,
              headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
          },
          success: function(success) {
            $("#confirm-delete-bank").modal("hide");
            location.reload(true);
            console.log("success");
          },
          error: function(errors) {
            console.table(errors);
          }
        });
    };

     $(".delete-card").click(function(e) {
        e.preventDefault();
        $("#confirm-delete-card").modal("show");
        var card_id = $(this).attr("id");
        $("#btn-card-ok").attr("href", "/payment/customer/delete/" + card_id);
        $("#btn-card-ok").click(function(e) {
          e.preventDefault();
          deleteCard($("#btn-card-ok").attr("href"));
        });

    });

    function deleteCard(_url){
        console.log(_url)
         $.ajax({
              type: "GET",
              url: _url,
              headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
          },
          success: function(success) {
            $("#confirm-delete-card").modal("hide");
            location.reload(true);
            console.log("success");
          },
          error: function(errors) {
            console.table(errors);
          }
        });
    };
  });
