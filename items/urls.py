from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import (
    ItemView,
    DeleteItemView,
    UpdateItemView
)

urlpatterns = [
    url(r'^$', ItemView.as_view(), name='api'),
    url(r'^delete$', DeleteItemView.as_view(), name='delete-item'),
    url(r'^update$', UpdateItemView.as_view(), name='update-item'),

]

urlpatterns = format_suffix_patterns(urlpatterns)
