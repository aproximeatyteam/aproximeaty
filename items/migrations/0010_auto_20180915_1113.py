# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-09-15 11:13
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('items', '0009_auto_20180915_1113'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='updated_at',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 9, 15, 11, 13, 57, 81663, tzinfo=utc)),
        ),
    ]
