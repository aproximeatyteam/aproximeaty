# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-09-10 17:38
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc
import django.utils.timezone
import items.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
                ('description', models.CharField(blank=True, default='', max_length=127)),
                ('unity', models.CharField(choices=[('METRE', 'METRE'), ('LITRE', 'LITRE'), ('GRAM', 'GRAM')], default='GRAM', max_length=5)),
                ('slug', models.SlugField(blank=True)),
            ],
            options={
                'verbose_name': 'Category',
                'verbose_name_plural': 'Categories',
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_available', models.BooleanField(default=False)),
                ('created_at', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('currency', models.CharField(choices=[('EUR', 'EUR'), ('USD', 'USD')], default='EUR', max_length=3)),
                ('description', models.CharField(blank=True, default='', max_length=127)),
                ('slug', models.SlugField(blank=True)),
                ('image', models.ImageField(blank=True, default='default.png', null=True, upload_to=items.models.upload_location)),
                ('is_the_day_item', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=20)),
                ('price', models.FloatField(default=0.0)),
                ('quantity', models.IntegerField(blank=True, default=0)),
                ('updated_at', models.DateTimeField(blank=True, default=datetime.datetime(2018, 9, 10, 17, 38, 16, 790157, tzinfo=utc))),
                ('tax', models.FloatField(blank=True, default=5.5)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='items.Category')),
                ('partner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='items', to='profiles.Partner')),
            ],
            options={
                'ordering': ('name',),
            },
        ),
    ]
