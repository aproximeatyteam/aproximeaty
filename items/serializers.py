from rest_framework import serializers
from .models import Item, Category


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = [
            'name',
            'quantity',
            'description',
            'price',
            'slug',
            'is_available',
            'image',
            'category'
        ]
        depth = 1


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = [
            'name',
            'description',
            'unity',
            'slug'
        ]
        depth = 1
