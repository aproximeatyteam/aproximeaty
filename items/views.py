from django.shortcuts import render, get_object_or_404
from .serializers import ItemSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from profiles.models import Partner
from .models import Item, Category
from django.utils.translation import ugettext_lazy as _


class ItemView(APIView):

    def get_object(self, slug):
        try:
            return Item.objects.get(slug=slug)
        except Item.DoesNotExist:
            return Response('error', status=status.HTTP_404_NOT_FOUND)

    def get(self, request):
        item = self.get_object(request.GET.get('slug'))
        serializer = ItemSerializer(item)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        try:
            if request.FILES.get('image', False):
                request.data['image'] = request.FILES['image']
                print(request.data['image'])

            itemSerializer = ItemSerializer(data=request.data)

            if itemSerializer.is_valid():
                category = get_object_or_404(Category, name=request.POST.get('category'))
                itemSerializer.validated_data['category'] = category
                partner = get_object_or_404(Partner, slug=request.user.slug)
                itemSerializer.validated_data['partner'] = partner
                itemSerializer.save()
                return Response('success', status=status.HTTP_201_CREATED)

            return Response(_('Please, check if you don\'t miss something :-)'),
                            status=status.HTTP_400_BAD_REQUEST)
        except ValueError:
            return Response(ValueError, status=status.HTTP_400_BAD_REQUEST)


class DeleteItemView(APIView):

    def get(self, request, *args, **kwargs):

        item = Item.objects.filter(slug=request.GET.get('slug'))
        try:
            item.delete()
            return Response('success', status=status.HTTP_202_ACCEPTED)
        except Exception as e:
            return Response('error', status=status.HTTP_409_CONFLICT)


class UpdateItemView(APIView):
    def get_object(self, slug):
        try:
            return Item.objects.get(slug=slug)
        except Item.DoesNotExist:
            return Response('error', status=status.HTTP_404_NOT_FOUND)

    def post(self, request, *args, **kwargs):
        try:
            if request.FILES.get('image', False):
                request.data['image'] = request.FILES['image']

            item = self.get_object(request.POST.get('slug'))
            serializer = ItemSerializer(item, data=request.data)
            print(request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

            return Response(
                _('Please, check if you don\'t miss something :-)'),
                status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            return Response(
                _('Please, check if you don\'t miss something :-)'),
                status=status.HTTP_400_BAD_REQUEST)