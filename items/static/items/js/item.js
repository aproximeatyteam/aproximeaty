$(document).ready(function() {
  "use strict";
  var csrftoken = $("input[name=csrfmiddlewaretoken]").val();

  //     Create new meal
  $("#createMealPopUp").on("click", function() {
    $("#create-item-form").trigger("reset");
    deletePreviewPicture("#create-item-form");
    $("#create-item-modal").modal("show");
    $("#create-item-modal #item-category").val("meal");
    $("#create-item-form .loading").hide();
    $("#create-item-error").hide();
  });

  //     Create new extra
  $("#createExtraPopUp").on("click", function() {
    $("#create-item-form").trigger("reset");
    deletePreviewPicture("#create-item-form");
    $("#create-item-form .loading").hide();
    $("#create-item-modal").modal("show");
    $("#create-item-modal #item-category").val("extra");
    $("#create-item-error").hide();

  });

  $("#create-item-form").on("submit", function(e) {
    e.preventDefault();
    $("#create-item-form .loading").show();
    $("#create-item-form .submit").hide();
    var create_item_Name = $("#create-item-form #item-name");
    var create_item_quantity = $("#create-item-form #item-quantity");
    var create_item_description = $("#create-item-form #item-description");
    var create_item_price = $("#create-item-form #item-price");
    var create_item_is_available = $("#create-item-form #item-is-available");
    var create_item_category = $("#create-item-form #item-category");
    var create_item_image = $("#create-item-form #item-image");

    var form_data = new FormData();
    form_data.append("name", create_item_Name.val());
    form_data.append("quantity", create_item_quantity.val());
    form_data.append("description", create_item_description.val());
    form_data.append("price", create_item_price.val());
    form_data.append("category", create_item_category.val());
    form_data.append("is_available", is_available("create-item-form"));
    form_data.append("image", create_item_image[0].files[0]);

    console.log(is_available("create-item-form"));
    $.ajax({
      type: "POST",
      url: "/item/",
      data: form_data,
      headers: {
        "X-CSRFToken": csrftoken
      },
      cache: false,
      contentType: false,
      processData: false,
      success: function(success) {
        $("#create-item-modal").modal("hide");
        location.reload(true);
        console.log("success");
      },
      error: function(error) {
        $("#create-item-form .loading").hide();
        $("#create-item-form .submit").show();
        $("#create-item-error").show();
        $("#create-item-error").html(error.responseText)
          console.table(error)
      }
    });
  });

  //    Update The meal
  $(".editItem").on("click", function() {
    var itemSlug = $(this)
      .closest("li")
      .attr("id");
    editItemForm(itemSlug);
  });

  var update_item_name = $("#update-item-form #item-name");
  var update_item_quantity = $("#update-item-form #item-quantity");
  var update_item_description = $("#update-item-form #item-description");
  var update_item_price = $("#update-item-form #item-price");
  var update_item_is_available = $("#update-item-form #is-available");
  var update_item_category = $("#update-item-form #item-category");
  var update_item_image = $("#update-item-form #item-image");
  var update_item_slug = $("#update-item-form #item-slug");

  function editItemForm(itemSlug) {
    $("#update-item-form").trigger("reset");
    deletePreviewPicture("#update-item-form");
    $("#update-item-modal").modal("show");
    $("#update-item-form .loading").hide();
    $("#update-item-error").hide();

    $.ajax({
      type: "GET",
      url: "/item?slug=" + itemSlug,
      success: function(item) {
        console.table(item);
        update_item_name.val(item.name);
        update_item_quantity.val(item.quantity);
        update_item_description.val(item.description);
        update_item_price.val(item.price);
        update_item_category.val(item.category.name);
        update_item_slug.val(item.slug);

        if (item.image) {
          addItemPicture(item.image, true, "#update-item-form");
        }

        if (item.is_available) {
          $("#update-item-form #is-available").prop("checked", true);
        }
      },
      error: function(xhr) {
        alert("error");
        alert(xhr);
        alert("error");
      }
    });
  }
  $("#update-item-form").on("submit", function(e) {
    e.preventDefault();
    var update_item_slug = $("#update-item-form #item-slug").val();
    $("#update-item-form .loading").show();
    $("#update-item-form .update").hide();
    updateItemForm(update_item_slug);
  });

  function updateItemForm(update_item_slug) {
    var form_data = new FormData();
    form_data.append("name", update_item_name.val());
    form_data.append("quantity", update_item_quantity.val());
    form_data.append("description", update_item_description.val());
    form_data.append("price", update_item_price.val());
    form_data.append("category", update_item_category.val());
    form_data.append("slug", $("#update-item-form #item-slug").val());
    form_data.append("is_available", is_available("update-item-form"));

    var img = $("#update-item-form #item-image")[0].files[0];
    if (img) {
      form_data.append("image", img);
    }

    $.ajax({
      type: "POST",
      url: "/item/update",
      data: form_data,
      headers: {
        "X-CSRFToken": csrftoken
      },
      cache: false,
      contentType: false,
      processData: false,
      success: function(success) {
        $("#update-item-form").modal("hide");
        location.reload(true);
        console.log("success");
      },
      error: function(error) {
        $("#update-item-form .loading").hide();
        $("#update-item-form .update").show();
        $("#update-item-error").show();
        $("#update-item-error").html(error.responseText)
      }
    });
  }
  var test = function(slug) {
    console.log(slug);
  };

  //    Delete Item
  $(".delete-item").on("click", function(e) {
    e.preventDefault();
    $("#confirm-delete").modal("show");
    var item_slug = $(this)
      .closest("li")
      .attr("id");
    $("#btn-ok").attr("href", "/item/delete?slug=" + item_slug);
    $("#btn-ok").click(function(e) {
      e.preventDefault();
      deleteItem($("#btn-ok").attr("href"));
    });

    console.log(item_slug);
  });

  var deleteItem = function(_url) {
    $.ajax({
      type: "GET",
      url: _url,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        "X-CSRFToken": csrftoken
      },
      success: function(success) {
        $("#confirm-delete").modal("hide");
        location.reload(true);
        console.log("success");
      },
      error: function(errors) {
        console.table(errors);
      }
    });
  };

  var is_available = function(formId) {
    var available = false;
    if (formId == "create-item-form") {
      if ($("#create-item-form #is-available").is(":checked")) {
        return (available = true);
      }
    } else if (formId == "update-item-form") {
      if ($("#update-item-form #is-available").is(":checked")) {
        return (available = true);
      }
    }

    return available;
  };

  function readURL(input, formId) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        addItemPicture(e.target.result, false, formId);
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  var addItemPicture = function(src, serverUrl = false, formId) {
    console.log(formId);
    $(formId + " #itemImgIcon").hide();
    $("#update-item-form  #itemPicture").remove();
    var img = $('<image id="itemPicture">');

    img.attr("src", src);

    img.attr("width", "200");
    img.attr("height", "200");

    img.appendTo(formId + " #itemImageBlock  #imageUpload");
    imgPreviewOnClick(formId);
  };

  var deletePreviewPicture = function(formId) {
    $(formId + " #itemPicture").remove();
    $(formId + " #itemImgIcon").show();
  };

  var imgPreviewOnClick = function(formId) {
    $(formId + " #itemPicture").on("click", function(e) {
      $(formId + " #item-image").click();
    });
  };

  $("#create-item-form #item-image").change(function() {
    readURL(this, "#create-item-form");
  });

  $("#update-item-form #item-image").change(function() {
    readURL(this, "#update-item-form");
  });
});
