$(document).ready(function(){
    'use strict';
    initSlides();
     // start sliders functions
    function initSlides() {
        if($( window ).width() < 768){
          $('.dishes-slides').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow : '<button type="button" class="slick-next"><img src="/static/assets/imgs/arrow-next.png"></button>',
            prevArrow : '<button type="button" class="slick-prev"><img src="/static/assets/imgs/arrow-prev.png"></i></button>'
          });
          $('.extras-slides').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow : '<button type="button" class="slick-next"><img src="/static/assets/imgs/arrow-next.png"></button>',
            prevArrow : '<button type="button" class="slick-prev"><img src="/static/assets/imgs/arrow-prev.png"></i></button>'
          });
        }else{
          $('.dishes-slides').slick({
            slidesToShow: 3,
            rows: 2,
            slidesToScroll: 1,
            nextArrow : '<button type="button" class="slick-next"><img src="/static/assets/imgs/arrow-next.png"></button>',
            prevArrow : '<button type="button" class="slick-prev"><img src="/static/assets/imgs/arrow-prev.png"></i></button>'
          });

          $('.extras-slides').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            nextArrow : '<button type="button" class="slick-next"><img src="/static/assets/imgs/arrow-next.png"></button>',
            prevArrow : '<button type="button" class="slick-prev"><img src="/static/assets/imgs/arrow-prev.png"></i></button>'
          });
        }
      }


    $( window ).resize(function() {
        console.log('resize: ' + $( window ).width());
        $('.dishes-slides').slick('unslick');
        $('.extras-slides').slick('unslick');
        initSlides();
    });
});
