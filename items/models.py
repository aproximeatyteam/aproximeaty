from enum import Enum
from django.db.models.signals import post_save, pre_save
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from util.utils import unique_slug_generator
from profiles.models import Partner


class Unity(Enum):
    METRE = "METRE"
    LITRE = "LITRE"
    GRAM = "GRAM"


def upload_location(instance, filename):
    return "item/%s/%s" %(instance.slug, filename)


class Category(models.Model):
    name = models.CharField(max_length=20, blank=False, null=False)
    description = models.CharField(max_length=127, blank=True, default='')
    unity = models.CharField(max_length=5,
                             choices=[(unit.name, unit.value) for unit in Unity],
                             default='GRAM')
    slug = models.SlugField(blank=True, null=False)

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    @property
    def title(self):
        return self.name

    def __str__(self):
        return str(self.name)

    def __unicode__(self):
        return str(self.name)


def pre_save_category_model_receiver(sender, instance,  **kwargs):
    instance.slug = unique_slug_generator(instance)


post_save.connect(pre_save_category_model_receiver, sender=Category)


class Currency(Enum):
    EUR = "EUR"
    USD = "USD"


class Item(models.Model):
    is_available = models.BooleanField(blank=True, default=False)
    created_at = models.DateTimeField(blank=True, default=timezone.now)
    currency = models.CharField(max_length=3,
                                choices=[(currency.name, currency.value) for currency in Currency],
                                default='EUR')
    description = models.CharField(max_length=140, blank=True, default="")
    slug = models.SlugField(blank=True, null=False)
    image = models.ImageField(
        upload_to=upload_location,
        default='default.png',
        blank=True,
        null=True
    )
    is_the_day_item = models.BooleanField(blank=True, default=False)
    name = models.CharField(max_length=20, blank=False, null=False)
    price = models.FloatField(blank=False, null=False, default=0.0)
    quantity = models.IntegerField(blank=True, default=0)
    updated_at = models.DateTimeField(blank=True, default=timezone.now())
    tax = models.FloatField(blank=True, default=5.5)

    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    partner = models.ForeignKey(Partner, on_delete=models.CASCADE, related_name='items')

    class Meta:
        ordering = ('name',)

    @property
    def title(self):
        return self.name

    def __str__(self):
        return str(self.name)

    def __unicode__(self):
        return str(self.name)


def pre_save_item_model_receiver(sender, instance,  **kwargs):
    instance.slug = unique_slug_generator(instance)



pre_save.connect(pre_save_item_model_receiver, sender=Item)