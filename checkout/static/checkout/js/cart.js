$(document).ready(function(){
    'use strict';
    var csrftoken = $('input[name=csrfmiddlewaretoken]').val()
    

    $('#add-to-cart-form').on('submit', function(e){
        e.preventDefault();
        console.log('#add-to-cart')
        var slug = $('#item-slug').val()
        var form = document.getElementById("add-to-cart-form"),
        form_data = new FormData(form)

        console.log(slug)
        $.ajax({
            type: 'POST',
            url: '/checkout/add-to-cart',
            data:form_data,
            headers: {
                'X-CSRFToken': csrftoken
            },
            cache: false,
            contentType: false,
            processData: false,
            success: function(success) {
                $('#orderDetailModal').modal('hide')
                location.reload(true);
            },
            error: function(error){
                $('#add-to-cart-error').show()
                $('#add-to-cart-error').html(error.responseText)
                console.table(error.responseText)
            }
        })
    })



});

// Cart popOver init function
$(function () {
    var popover = $('#cartPopUp');
    if (popover != null) {
        popover.hide();

        tippy('#cartPopUpbtn', {
            html: '#cartPopUp',
            placement: 'bottom-end',
            delay: [200, 500],
            arrow: true,
            arrowType: 'round',
            interactive: true,
        })
    }
});

function deleteCartItem(pk) {
    console.log('Delete cart item');
    $.ajax({
        type: 'GET',
        url: '/checkout/delete-cart-item/' + pk,
        success: function (success) {
            location.reload(true);
        },
        error: function (error) {

            console.table(error.responseText)
        }
    })
}

function openOrderModal(item_slug, item_name){
    console.log('add to cart')
    $('#orderDetailModal').modal('show')
    $('#orderDetailModal #item-slug').val(item_slug)
    $('#orderDetailModal #item-name').html(item_name)
    $('#add-to-cart-error').hide()
    console.log(item_slug)
    console.log(item_name)
}

