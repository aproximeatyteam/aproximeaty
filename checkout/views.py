from django.utils import timezone
from django.shortcuts import render
from django.views.generic import CreateView, View, DeleteView
from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import (
    ShoppingCart,
    OrderDetail,
    Payment,
    Order,
    Subscription
)
from items.models import Item
from profiles.models import Customer, Partner


from .utils import (
    get_order_details_sum,
    reset_cart,
    shopping_cart_to_session,
    add_checkout_info_to_context,
    send_money_to_partners,
    checkout_page_context
)


class AddToCartView(APIView):

    def post(self, request, *args, **kwargs):

        if request.user.is_anonymous():
            return Response(_('Please, login-in as a customer to add this item to the cart'),
                            status=status.HTTP_401_UNAUTHORIZED)
        elif request.user.profile == 'partner':
            return Response(_('Please, login with customer account'), status=status.HTTP_409_CONFLICT)
        else:
            shopping_cart = shopping_cart_to_session(request)
            if shopping_cart is not None:

                if request.POST.get('instruction'):
                    instruction = request.POST.get('instruction').strip()
                else:
                    instruction = ' '

                item = Item.objects.get(slug=request.POST.get('slug').strip())
                print(request.POST.get('slug'))

                OrderDetail.objects.create(
                    shopping_cart=shopping_cart,
                    item=item,
                    quantity=request.POST.get('quantity'),
                    instruction=instruction
                )
                request.session['shopping_cart'] = shopping_cart.slug

        return Response('Success', status=status.HTTP_200_OK)


class DelItemCartView(APIView):

    def get(self, request, *args, **kwargs):

        OrderDetail.objects.filter(pk=kwargs.get('pk')).delete()

        return Response('Success', status=status.HTTP_200_OK)


class CheckoutView(CreateView):
    template_name = 'checkout/checkout.html'

    def get(self, request, *args, **kwargs):

        context = checkout_page_context(request)
        return render(
            request,
            template_name=self.template_name,
            context=context
        )

    def post(self, request, *args, **kwargs):
        try:
            shopping_cart = ShoppingCart.objects.get(slug=request.session['shopping_cart'])
            customer = shopping_cart.customer
            order_details = shopping_cart.order_details.all()

            card_id = request.POST.get('card-id')

            if card_id is not None:
                print('card_id is not None')
                send_money_to_partners(order_details, stripe_customer=customer.payment_id, card_id=card_id)

                delivered_address = request.POST.get('address') or ' '

                additional_address_info = request.POST.get('additional-address-info') or ' '
                instruction = request.POST.get('instruction') or ' '
                order = Order(
                    delivered_address=delivered_address,
                    customer=customer,
                    payment=None,
                    instruction=instruction,
                    additional_address_info=additional_address_info
                )
                order.save()
                order.order_details = order_details
                order.save()
                reset_cart(shopping_cart.slug, request)

                return HttpResponseRedirect(reverse_lazy('profile:partner_list'))
            else:
                context = checkout_page_context(request)
                context['error'] = _('Please, select one card to purchase with :-)')
                return render(
                    request,
                    template_name=self.template_name,
                    context=context
                )
        except Exception as e:
            print(e)
            context = checkout_page_context(request)
            context['error'] = _('Please verify all details before submitting :-)')
            return render(
                request,
                template_name=self.template_name,
                context=context
            )
