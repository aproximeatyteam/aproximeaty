from checkout.models import ShoppingCart
from profiles.models import Customer
from payment.utils import (
    send_money_to_partner,
    send_money_with_shared_cust,
    get_stripe_customer_acct
)


def checkout_page_context(request):
    customer = Customer.objects.get(id=request.user.id)
    stripe_customer_acct = get_stripe_customer_acct(customer)
    context = add_checkout_info_to_context(request, context={})
    context['stripe_customer_acct'] = stripe_customer_acct
    return context


def get_cart_subtotal(request, context):
    sub_total = 0.0
    shopping_cart_set = ShoppingCart.objects.filter(customer__id=request.user.id)
    if shopping_cart_set.exists():
        context['shopping_cart'] = shopping_cart_set.first()
        for order_detail in context['shopping_cart'].order_details.all():
            sub_total += order_detail.item.price * order_detail.quantity
    return round(sub_total, 2)


def add_checkout_info_to_context(request, context):
    if not request.user.is_anonymous() and request.user.profile == 'customer':
        sub_total = get_cart_subtotal(request, context)
        context['sub_total'] = sub_total
        context['shipping_fee'] = 5.00
        context['total'] = context['sub_total'] + context['shipping_fee']
    return context


def get_order_details_sum(order_details):
    sum=0.0
    for order_detail in order_details:
        sum += order_detail.quantity * order_detail.item.price * order_detail.item.tax

    return sum


def reset_cart(shopping_cart, request):
    cart = ShoppingCart.objects.filter(slug=shopping_cart).first()

    if cart is not None:
        cart.delete()
        del request.session['shopping_cart']
        request.session.modified = True


def get_partners(order_details):
    partners = []
    for order_detail in order_details:
        if not order_detail.item.partner in partners:
            partners.append(order_detail.item.partner)
    return partners


def get_subtotal_for_partner(order_details, partner_id):
    sum=0
    order_details = [order_detail for order_detail in order_details if order_detail.item.partner.id == partner_id]
    for order_detail in order_details:
        sum += order_detail.quantity * order_detail.item.price
    return sum


def send_money_to_partners(order_details, token=None, stripe_customer='', card_id=''):
    partners = get_partners(order_details)
    for partner in partners:
        sum = get_subtotal_for_partner(order_details, partner.id)
        if token is not None:
            print('token is not None')
            send_money_to_partner(sum, token, partner.payment_id)
        else:
            print('token is  None')
            send_money_with_shared_cust(sum, stripe_customer, partner.payment_id, card_id)


def shopping_cart_to_session(request):
    try:
        if not request.user.is_anonymous() and request.user.profile == 'customer':
            customer = Customer.objects.get(id=request.user.id)
            shopping_cart = ShoppingCart.objects.get(customer__id=request.user.id)
            request.session['shopping_cart'] = shopping_cart.slug
            return shopping_cart
        else:
            return None

    except ShoppingCart.DoesNotExist:
        return ShoppingCart.objects.create(customer=customer)
