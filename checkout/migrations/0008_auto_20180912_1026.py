# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2018-09-12 10:26
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('checkout', '0007_auto_20180911_1537'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='created_at',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 9, 12, 10, 26, 36, 537461, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='payment',
            name='created_at',
            field=models.DateField(blank=True, default=datetime.datetime(2018, 9, 12, 10, 26, 36, 532460, tzinfo=utc), null=True),
        ),
        migrations.AlterField(
            model_name='shoppingcart',
            name='created_at',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 9, 12, 10, 26, 36, 538461, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='subscription',
            name='created_at',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2018, 9, 12, 10, 26, 36, 535460, tzinfo=utc), null=True),
        ),
    ]
