from django.contrib import admin
from .models import (
    SavingAccount,
    Payment,
    Subscription,
    Order,
    ShoppingCart,
    OrderDetail
)

admin.site.register(SavingAccount)
admin.site.register(Payment)
admin.site.register(Subscription)
admin.site.register(Order)
admin.site.register(ShoppingCart)
admin.site.register(OrderDetail)

