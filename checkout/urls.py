from django.conf.urls import url
from .views import (
    AddToCartView,
    CheckoutView,
    DelItemCartView,
)
urlpatterns = [
    url(r'^add-to-cart$', AddToCartView.as_view(), name='add-to-cart'),
    url(r'^delete-cart-item/(?P<pk>\d+)$', DelItemCartView.as_view(), name='delete-cart-item'),
    url(r'^order$', CheckoutView.as_view(), name='order'),

]

