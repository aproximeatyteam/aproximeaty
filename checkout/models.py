from enum import Enum
from django.db import models
from django.utils import timezone
from django.db.models.signals import post_save, pre_save
from util.utils import unique_slug_generator
from items.models import Item
from profiles.models import Customer, Partner


class Payment(models.Model):
    amount = models.FloatField(blank=True, null=True, default=0)
    created_at = models.DateField(blank=True, null=True, default=timezone.now())
    transaction = models.CharField(max_length=20, blank=True, null=True)

    @property
    def title(self):
        return self.amount

    def __str__(self):
        return str(self.amount)

    def __unicode__(self):
        return str(self.amount)


class SavingAccount(models.Model):
    balance = models.FloatField(blank=True, default=0.0)
    partner = models.ForeignKey(Partner, blank=True, null=True)
    @property
    def title(self):
        return self.balance

    def __str__(self):
        return str(self.balance)

    def __unicode__(self):
        return str(self.balance)


class Subscription(models.Model):
    type = models.CharField(max_length=8, choices=[
                                            ('bronze', 'bronze'),
                                            ('silver', 'silver'),
                                            ('gold', 'gold')
                                            ],
                                            default='bronze'
                            )
    created_at = models.DateTimeField(blank=True, null=True, default=timezone.now())
    payment = models.OneToOneField(Payment, null=True)
    partner = models.ForeignKey(Partner, null=True, related_name='partner')

    @property
    def title(self):
        return self.type

    def __str__(self):
        return str(self.type)

    def __unicode__(self):
        return str(self.type)


class OrderStatus(Enum):
    CREATED = "CREATED"
    PAID = "PAID"
    SHIPPING = "SHIPPING"
    DELIVERED = "DELIVERED"


class Order(models.Model):
    class Meta:
        ordering = ['-id']
    order_status = models.CharField(max_length=9,
                                    choices=[(status.name, status.value) for status in OrderStatus],
                                    default='CREATED'
                                    )
    created_at = models.DateTimeField(blank=True, default=timezone.now())
    delivered_address = models.CharField(max_length=255, blank=True, null=True, default='')
    instruction = models.CharField(max_length=255, blank=True, default=' ')
    additional_address_info = models.CharField(max_length=255, blank=True, default=' ')

    customer = models.ForeignKey(Customer, null=True)
    payment = models.OneToOneField(Payment, null=True)

    @property
    def title(self):
        return self.order_status

    def __str__(self):
        return str(self.order_status)

    def __unicode__(self):
        return str(self.order_status)


class ShoppingCart(models.Model):
    created_at = models.DateTimeField(blank=True, default=timezone.now())
    is_completed = models.BooleanField(blank=True, default=False)
    slug = models.SlugField(blank=True, null=True)

    customer = models.OneToOneField(Customer, null=False)

    def removeCompleted(self):
        pass

    @property
    def title(self):
        return self.customer.first_name

    def __str__(self):
        return str(self.customer.first_name)

    def __unicode__(self):
        return str(self.customer.first_name)


def pre_save_shopping_cart_model_receiver(sender, instance, **kwargs):
    if instance.pk is None:
        instance.slug = unique_slug_generator(instance)


pre_save.connect(pre_save_shopping_cart_model_receiver, sender=ShoppingCart)


class OrderDetail(models.Model):
    class Meta:
        ordering = ['-id']

    quantity = models.IntegerField(blank=True, default=0)
    instruction = models.CharField(max_length=255, blank=True, default=' ')

    item = models.ForeignKey(Item, on_delete=models.CASCADE, blank=False, null=False)
    order = models.ForeignKey(Order,
                              on_delete=models.CASCADE,
                              blank=True,
                              null=True,
                              related_name='order_details')

    shopping_cart = models.ForeignKey(ShoppingCart,
                                      blank=True,
                                      null=True,
                                      related_name='order_details',
                                      on_delete=models.SET_NULL

                                      )

    def calculateSubTotal(self):
        pass

    @property
    def title(self):
        return self.quantity

    def get_total_price(self):
        return self.quantity * self.item.price

    def __str__(self):
        return str(self.quantity)

    def __unicode__(self):
        return str(self.quantity)

