# NOTICE OF LICENSE
# This file is licenced under the Software License Agreement.
# With the purchase or the installation of the software in your application
# you accept the licence agreement.
#
# You must not modify, adapt or create derivative works of this source code
#
# @author: EL AKIOUI Zouhaire
# @copyright 2018
# @license   licence.txt


from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static
from django.conf.urls import handler404, handler500
from django.conf import settings
from aproximeaty import views as common_view


urlpatterns = [
    url(r'^', include('home.urls', namespace='home')),
    url(r'^admin/', admin.site.urls),
    url(r'^home/', include('home.urls', namespace='home')),
    url(r'^profile/', include('profiles.urls', namespace='profile')),
    url(r'^account/', include('accounts.urls', namespace='account')),
    url(r'^dashboard/', include('dashboard.urls', namespace='dashboard')),
    url(r'^item/', include('items.urls', namespace='item')),
    url(r'^checkout/', include('checkout.urls', namespace='checkout')),
    url(r'^payment/', include('payment.urls', namespace='payment')),
]

# urlpatterns += staticfiles_urlpatterns()
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
handler404 = common_view.handler404
handler500 = common_view.handler500
