# NOTICE OF LICENSE
# This file is licenced under the Software License Agreement.
# With the purchase or the installation of the software in your application
# you accept the licence agreement.
#
# You must not modify, adapt or create derivative works of this source code
#
# @author: EL AKIOUI Zouhaire
# @copyright 2018
# @license   licence.txt

import random
import string
from django.utils.text import slugify
from django.conf import settings

SHORTCODE_MIN = getattr(settings, "SHORTCODE_MIN", 35)


def code_generator(size=SHORTCODE_MIN, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def random_string_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def unique_slug_generator(instance, new_slug=None):

    if new_slug is not None:
        slug = new_slug
    else:
        slug = slugify(instance.title)

    Klass = instance.__class__
    qs_exists = Klass.objects.filter(slug=slug).exists()
    if qs_exists:
        new_slug = "{slug}-{randstr}".format(
                    slug=slug,
                    randstr=random_string_generator(size=4)
                )
        return unique_slug_generator(instance, new_slug=new_slug)
    return slug


def add_to_session(request, user):
    request.session['user_slug'] = user.slug
    request.session['user_type'] = user.__class__.__name__
