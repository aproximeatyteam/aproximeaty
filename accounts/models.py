from enum import Enum
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser, Group
from .managers import AproxUserManager


def upload_location(instance, filename):
    return "account/%s/%s" %(instance.slug, filename)


class AproxUser(AbstractUser):
    slug = models.SlugField(blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True)
    address = models.CharField(_('Address'), max_length=120, blank=False, null=False, default='')
    zip_code = models.CharField(_('Zip code'), max_length=120, blank=False, null=False)
    city = models.CharField(_('City'), max_length=40, blank=False, null=False, default='')
    country = models.CharField(_('Country'), max_length=40, blank=False, null=False, default='')
    is_email_checked = models.BooleanField(
        _('Email checked'),
        default=False,
        help_text=_(
            'Designates whether this user checked his email. '
        ),
    )
    activate_key = models.CharField(max_length=120, blank=True, null=True, default='')
    image = models.ImageField(
        upload_to=upload_location,
        default='default.png',
        blank=True,
        null=True
    )
    phone_number = models.CharField(_('Phone number'), max_length=15, blank=False, null=False, default='')
    profile = models.CharField(null=False, max_length=8, default='Customer')


    manager = AproxUserManager


    def get_full_name(self):

        return self.email

    def get_short_name(self):
        return self.email

    def __str__(self):
        return self.email

    def verifyEmail(self):
        return self

    def get_profile(self):
        return self.profile

