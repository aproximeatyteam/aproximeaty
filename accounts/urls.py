from django.conf.urls import url
from django.urls import reverse_lazy
from django.contrib.auth.views import (
    LogoutView,
    PasswordResetDoneView,
    PasswordChangeDoneView,
    PasswordResetCompleteView
)

from .views import (
    UserPasswordChangeView,
    UserPasswordResetView,
    UserPasswordResetConfirmView,
    UserPasswordResetDoneView
)

urlpatterns = [
    url(r'^logout/$', LogoutView.as_view(next_page=reverse_lazy('profile:login')), name='logout'),
    url(r'^password_change/$',
        UserPasswordChangeView.as_view(),
        name='password_change'),

    url(r'^password_change/done/$',
        UserPasswordResetDoneView.as_view(
            template_name='accounts/password_change_done.html',
        ),
        name='password_change_done'),

    url(r'^password_reset/$',
        UserPasswordResetView.as_view(),
        name='password_reset'),

    url(r'^password_reset/done/$',
        PasswordResetDoneView.as_view(template_name='accounts/password_reset_done.html'),
        name='password_reset_done'),

    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        UserPasswordResetConfirmView.as_view(),
        name='password_reset_confirm'),

    url(r'^reset/done/$',
        PasswordResetCompleteView.as_view(template_name='accounts/password_reset_complete.html'),
        name='password_reset_complete'),

]