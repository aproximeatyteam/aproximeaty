from django.shortcuts import render, get_object_or_404
from django.views.generic import View, UpdateView
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from checkout.models import OrderDetail, SavingAccount
from .utils import get_partner_dashboard, get_customer_board


class DashboardView(LoginRequiredMixin, View):
    template_name = 'dashboard/partner/partner.html'

    def get(self, request, *args, **kwargs):
        try:
            if request.user.profile == 'partner':
                context = get_partner_dashboard(request)
                return render(request,
                              template_name='dashboard/partner/partner.html',
                              context=context
                              )

            if request.user.profile == 'customer':
                order_details = get_customer_board(request)
                return render(request,
                              template_name='dashboard/customer/customer.html',
                              context={'order_details': order_details})

            return HttpResponseRedirect(reverse_lazy('profile:partner_list'))
        except Exception as e:
            print(e)
            pass


