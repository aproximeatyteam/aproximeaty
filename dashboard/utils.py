from checkout.models import OrderDetail


def get_partner_orders(request):
    order_details = OrderDetail.objects.filter(item__partner__id=request.user.id).exclude(order__id__isnull=True)
    print(order_details)
    return order_details


def get_pending_orders(order_details):

    pending_orders = 0
    for order_detail in order_details:
        print(order_detail.order.id)
        if order_detail.order.order_status is not 'DELIVERED':
            pending_orders += 1
    return pending_orders


def get_partner_dashboard(request):
    order_details = get_partner_orders(request)
    pending_orders = get_pending_orders(order_details)
    context = {'order_details': order_details,
                'pending_orders': pending_orders,
              }
    return context


def get_customer_board(request):
    return OrderDetail.objects.filter(order__customer__id=request.user.id)
